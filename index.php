<?php

require 'application/lib/Dew.php';

use application\core\Router;
use application\core\Db;

define('ROOT', __DIR__ );

$protocol = "http://";
if (isset($_SERVER['HTTPS'])) {
    $protocol = 'https://';
}

//define("BASE_URL", $protocol.$_SERVER['SERVER_NAME']."/");
define('PASSWORD_LEN', 8);
define("BASE_URL", "/");

spl_autoload_register(function ($class) {
    $path = str_replace('\\', '/', $class. '.php');
    if (file_exists($path)){
        require $path;
    }
});

session_start();

require_once (ROOT .'/application/config/setup.php');

$router = new Router();
$router->run();
