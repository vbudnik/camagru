<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

function debug($str){
    echo '<pre>';
    var_dump($str);
    echo '</pre>';
    exit;
}

function reportLog($str){
    $file = ROOT."/var/log/debug.log";
    file_put_contents($file, "[". date('m/d/Y h:i:s a', time()) . "] ", FILE_APPEND | LOCK_EX);
    file_put_contents($file, json_encode($str)."\n", FILE_APPEND | LOCK_EX);
}

function mailerLog($str){
    $file = ROOT."/var/log/mail.log";
    file_put_contents($file, "[". date('m/d/Y h:i:s a', time()) . "] ", FILE_APPEND | LOCK_EX);
    file_put_contents($file, json_encode($str)."\n", FILE_APPEND | LOCK_EX);
}