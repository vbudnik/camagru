<?php

namespace application\core;

use \PDO;
use \PDOException;

class Db
{
    protected $connect;

    public static function getConnection()
    {
        if (!isset($connect)) {
            $paramPath = 'application/config/database.php';
            $params = include($paramPath);
            $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
            try {
                $connect = new PDO($dsn, $params['user'], $params['password']);
            } catch (PDOException $db) {
                reportLog($db->getMessage());
            }

            $connect->exec("set names utf-8");

        }
        return $connect;
    }
}
