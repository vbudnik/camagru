<?php

$paramPath = ROOT. "/application/config/database.php";


if (file_exists($paramPath)) {
    $params = include ($paramPath);
    $dsn = "mysql:host={$params['host']};";
    try {
        $connect = new PDO($dsn, $params['user'], $params['password']);
        $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $checkExistSQL = "show databases like '" . $params['dbname'] . "';";
        $result = $connect->query($checkExistSQL);
        if (!$result->fetchAll()) {
            $sql = "CREATE DATABASE IF NOT EXISTS ".$params['dbname'];
            $result = $connect->exec($sql);
            $connect = null;

            $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
            $connect = new PDO($dsn, $params['user'], $params['password']);
            $generalSQL = file_get_contents(ROOT.'/camagru.sql');
            $result = $connect->exec($generalSQL);
            $connect = null;
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit ;
    }
}