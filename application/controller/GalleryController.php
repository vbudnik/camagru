<?php

namespace application\controller;

use application\core\Controller;
use application\core\Pagination;
use application\models\Account;
use application\models\Gallery;
use application\models\User;

class GalleryController extends Controller
{
    public function indexAction()
    {
        if (User::checkLogin()) {
            $lastId = Gallery::getMaxId();
            $count = 0;
            for ($i = 0; $i <= $lastId[0]; $i++) {
                if (file_exists(ROOT.'/public/img/photos/'. $i .'.jpg')) {
                    $count++;
                }
            }
            $totalCount = ($count == $lastId[0]) ? $lastId[0] : $count;
            $pagination = new Pagination($this->route, $totalCount);
            $allData = Gallery::getAllPhoto($this->route);
            $like = [];
            foreach ($allData as $key => $val) {
                if ($val['likes'] == null) {
                    $like[] = 0;
                } else {
                    $count = 0;
                    $tmp = json_decode($val['likes'], true);
                    foreach ($tmp as $key => $val) {
                        if ($val['like'] == 1) {
                            $count++;
                        }
                    }
                    $like[] = $count;
                }
            }
            $vars =[
                'like'          =>  $like,
                'list'          =>  json_encode($allData),
                'pagination'    =>  $pagination->get(),
            ];
            $this->view->render('Camagru | Gallery', $vars);
            return true;
        }
        $this->view->redirect('/');
        return true;
    }

    public function commentAction()
    {
        if (User::checkLogin()) {
            if (isset($_POST['subject'])) {
                $comment = $_POST['subject'];
                $url = $_POST["url"];
                $token = explode("/", $url);
                $photoId = $token[4];
                $userId = User::getUserId();
                $ownerId = Gallery::getOwnerPhoto($photoId);
                Gallery::addComment($comment, $userId, $photoId, $ownerId);
                $ownerEmail = User::getUserEmailById($ownerId);
                if (User::checkNotification($userId)) {
                    Account::sendNotificationEmail($ownerEmail, $photoId);
                }
                $author = User::getNameFromId($userId);
                echo json_encode([
                    'comment' => $comment,
                    'author'    =>  $author
                ]);
                return true;
            }
        }
        $this->view->redirect('/');
        return false;
    }

    public function likeAction()
    {
        if (User::checkLogin() && !empty($_POST)) {
            if (isset($_POST['likes'])) {
                $userId = User::getUserId();
                $like = $_POST['likes'];
                $url = $_POST["url"];
                $token = explode("/", $url);
                $photoId = $token[4];
                $allLikesJson = Gallery::getAllLikes($photoId);
                $allLikes = json_decode($allLikesJson, true);
                $result = [];
                if (is_array($allLikes)) {
                    $flag = 0;
                    foreach ($allLikes as $key => $val) {
                        if ($val['user_id'] == $userId) {
                            $flag = 1;
                            $result[] = [
                                "user_id" => $val['user_id'],
                                "like"  => $like
                            ];
                        } else {
                            $result[] = [
                                "user_id" => $val['user_id'],
                                "like"  => $val['like']
                            ];
                        }
                    }
                    if ($flag != 1) {
                        $tmp = [
                            "user_id" => $userId,
                            "like"  => $like
                        ];
                        array_push($result, $tmp);
                    }
                } else {
                    $tmp = [
                        "user_id" => $userId,
                        "like"  => $like
                    ];
                    array_push($result, $tmp);
                }
                $likeJson = json_encode($result);
                Gallery::updateLike($likeJson, $photoId);
                if ($like == 1) {
                    echo json_encode("Problem with server! Contact with developer!!!");
                    return true;
                }
                return true;
            }
            return false;
        }
        $this->view->redirect('/');
        return true;
    }

    public function photoAction()
    {
        if (User::checkLogin()) {
            $photoId = $this->route['id'];
            $userId = User::getUserId();
            $allComment = Gallery::getAllComment($photoId);
            $commentData = json_encode($allComment);
            $allLikesJson = Gallery::getAllLikes($photoId);
            $allLikes = json_decode($allLikesJson, true);
            $like = 0;
            $count = 0;
            if (is_array($allLikes) || gettype($allLikes) == 'object') {
                foreach ($allLikes as $key => $val) {
                    if ($val['like'] == 1) {
                        $count++;
                    }
                    if ($val['user_id'] == $userId) {
                        if ($val['like'] == 1) {
                            $like = 1;
                        }
                    }
                }
            }
            $vars = [
                'if_user_like'  =>  $like,
                'like'          =>  $count,
                'data'          =>  $commentData,
                'img'           =>  '/public/img/photos/'.$photoId.'.jpg',
            ];
            $this->view->render('Camagru | Photo', $vars);
            return true;
        }
        $this->view->redirect('/');
        return true;
    }

    public function addAction()
    {
        if (User::checkLogin()
            && isset($_POST['image'])
            && isset($_POST['name'])
            && isset($_POST['description'])) {
            try {
                $string = preg_replace('~data:image/png;base64,~', '', $_POST['image']);
                $name = $_POST['name'];
                $description = $_POST['description'];
                $image = base64_decode($string);
                $options = [];
                $options['owner_id'] = User::getUserId();
                $options['name_img'] = $name;
                $options['description'] = $description;
                $id = Gallery::addPhoto($options);
                if (!$id) {
                    echo json_encode("Problem with server! Contact with developer!!!");
                    return false;
                }
                if (!file_exists(ROOT . "/public/img/photos")) {
                    mkdir(ROOT . "/public/img/photos", 0777);
                }
                $file = ROOT. "/public/img/photos/{$id}.jpg";
                try {
                    file_put_contents($file, $image);
                } catch (\Exception $exception) {
                    reportLog($exception->getMessage());
                }
                if (!file_exists($file)) {
                    echo json_encode("Problem with server! Contact with developer!!!");
                    return false;
                }
                return true;
            } catch (\Exception $e) {
                reportLog($e->getMessage());
            }
        }
        $this->view->redirect('/');
        return false;
    }
}
