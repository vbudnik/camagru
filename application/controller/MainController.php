<?php

namespace application\controller;

use application\core\Controller;

use application\core\Db;
use application\models\Main;
use application\models\User;

class MainController extends Controller
{
    public function indexAction()
    {
        if (!isset($_SESSION['user'])) {
            $passwors = '';
            $email = '';
            $errors = false;
            if (isset($_POST['email']) && isset($_POST['password'])) {
                $passwors = $_POST['password'];
                $email = $_POST['email'];
                if (!User::checkEmail($email)) {
                    $errors[] = 'Not correct email!';
                }
                if (!User::checkEmailExists($email)) {
                    $errors[] = 'Email not exist!';
                }
                if (!User::checkPassword($passwors)) {
                    $errors[] = 'Not correct password!';
                }
                if (!User::checkUserStatus($email, $passwors)) {
                    $errors[] = 'User not confirmed or not correct password!';
                }
                if ($errors == false) {
                    $userId = User::checkUserData($email, $passwors);
                    if (!$userId) {
                        $errors[] = 'Not correct password!';
                    } else {
                        User::auth($userId);
                        return true;
                    }
                }
                if ($errors != false) {
                    echo json_encode($errors);
                    return false;
                }
            }
            $this->view->render('Camagru | Home');
            return true;
        }
        $this->view->redirect('/gallery');
        return true;
    }

    public function logoutAction()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            $this->view->redirect('/');
        }
        return true;
    }
}
