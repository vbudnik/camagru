<?php

namespace application\controller;

use application\core\Controller;

class ContactController extends Controller
{
    public function indexAction ()
    {
        $this->view->render('Camagru | Contact');
    }
}
