<?php

namespace application\models;

use application\core\Db;
use PDO;

class Gallery
{
    const SHOW_BY_DEFAULT = 5;

    public static function addComment($comment, $userId, $photoId, $ownerId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'INSERT INTO comments (id_img, id_sender, id_owner, comment_text) VALUES  (?, ?, ?, ?)';

            $result = $db->prepare($sql);
            $result->execute(array($photoId, $userId, $ownerId, $comment));
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function addPhoto($options)
    {
        try {
            $db = Db::getConnection();
            $sql = 'INSERT INTO images  (owner_id, name_img, description_img) VALUES  (?, ?, ?)';

            $result = $db->prepare($sql);
            $result = $result->execute(array($options['owner_id'], $options['name_img'],  $options['description']));
            if ($result) {
                return $db->lastInsertId();
            }
            return 0;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getMaxId()
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT MAX(id) FROM images;';

            $result = $db->prepare($sql);
            $result->execute();
            return $result->fetch();
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getOwnerPhoto($id)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT owner_id FROM images WHERE id='.$id;

            $result = $db->prepare($sql);
            $result->execute();
            $ownerId = $result->fetch();
            return $ownerId['owner_id'];
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getAllComment($photoId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT id_img, id_sender, id_owner, comment_text, comment_data FROM comments WHERE id_img = ?'
            .'ORDER BY comment_data DESC;';

            $result = $db->prepare($sql);
            $result->execute(array($photoId));
            $i = 0;
            $commentList = array();
            while ($row = $result->fetch()){
                $commentList[$i]['name_sender'] = User::getNameFromId($row['id_sender']);
                $commentList[$i]['name_owner'] = User::getNameFromId($row['id_owner']);
                $commentList[$i]['comment_text'] = $row['comment_text'];
                $commentList[$i]['comment_data'] = $row['comment_data'];
                $i++;
            }
            return $commentList;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getAllLikes($photoId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT likes FROM images WHERE id='.$photoId;

            $result = $db->prepare($sql);
            $result->execute();
            $likes = $result->fetch();
            return $likes['likes'];
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getAllPhoto($routs)
    {
        try {
            $max = 5;
            if (isset($routs['page'])){
                $start = ($routs['page'] - 1) * $max;
            } else {
                $start = 0;
            }
            $db = Db::getConnection();
            $start = intval($start);
            $max = intval($max);
            $sql = 'SELECT * FROM images ORDER BY id DESC LIMIT '.$start.','.$max.';';

            $result = $db->prepare($sql);
            $result->execute();
            $i = 0;
            $imageList = array();
            while ($row = $result->fetch()){
                $imageList[$i]['id'] = $row['id'];
                $imageList[$i]['likes'] = $row['likes'];
                $imageList[$i]['owner_id'] = $row['owner_id'];
                $imageList[$i]['name_img'] = $row['name_img'];
                $imageList[$i]['description_img'] = $row['description_img'];
                $i++;
            }
            return $imageList;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function updateLike($likeJson, $photoId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE images SET likes = :likes WHERE id = :photoId;';

            $result = $db->prepare($sql);
            $result->bindParam(':likes', $likeJson, PDO::PARAM_INT);
            $result->bindParam(':photoId', $photoId, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

}
