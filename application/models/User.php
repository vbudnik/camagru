<?php

namespace application\models;

use application\core\Db;
use \PDO;
use \PDOException;

class User
{
    public static function checkPassword($password)
    {
        if (strlen($password) >= 4){
            return true;
        }
        return false;
    }

    public static function checkEmailExists($email)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT count(*) FROM user WHERE email = :email';

            $result = $db->prepare($sql);
            $result->bindParam(':email', $email, PDO::PARAM_STR);
            $result->execute();
            if ($result->fetchColumn()){
                return true;
            }
            return false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }

    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    public static function checkLogin()
    {
        if (isset($_SESSION['user'])){
            return true;
        }
        return false;
    }

    public static function checkUserStatus($email, $password)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE email=:email AND password = :password';

            $result = $db->prepare($sql);
            $password = hash('whirlpool', $password);
            $result->bindParam(':email', $email, PDO::PARAM_INT);
            $result->bindParam(':password', $password, PDO::PARAM_INT);
            $result->execute();
            $user = $result->fetch();
            return $user ? $user['status'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function checkUserData($email, $password)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE email=:email AND password = :password';

            $result = $db->prepare($sql);
            $password = hash('whirlpool', $password);
            $result->bindParam(':email', $email, PDO::PARAM_INT);
            $result->bindParam(':password', $password, PDO::PARAM_INT);
            $result->execute();
            $user = $result->fetch();
            return $user ? $user['id'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getUserId() {
        return $_SESSION['user'];
    }

    public static function registerUser($options)
    {
        try {
            $db = Db::getConnection();
            $sql = 'INSERT INTO user (name, email, password, status)'
                .'VALUES (?, ?, ?, 0);';

            $pasword = hash('whirlpool', $options['password']);
            $result = $db->prepare($sql);
            $result = $result->execute(array($options['name'], $options['email'], $pasword));
            if ($result) {
                return $db->lastInsertId();
            }
            return 0;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getNameFromId($userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT name FROM user WHERE id=?';

            $result = $db->prepare($sql);
            $result->execute(array($userId));
            $row = $result->fetch();
            return $row['name'];
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function setAccessToken($token, $userId) {
        try {
            $db = Db::getConnection();
            $sql = "INSERT INTO access_token (token, status, user_id)"
                ." VALUES ('".$token."', '0', ".strval($userId).");";

            $result = $db->prepare($sql);
            $result = $result->execute();
            if ($result) {
                return $db->lastInsertId();
            }
            return 0;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function sendRegistrationEmail($email, $token)
    {
        try {
            $to_email = $email;
            $subject = 'Accept you registration';
            $url = "http://localhost:8080/access/" . strval($token);
            $message = "This mail is sent using the PHP mail function\n" . $url;
            $headers = 'From: noreply@camagru.com';
            $success = mail($to_email, $subject, $message, $headers);
            if (!$success) {
                $errorMessage = error_get_last()['message'];
                mailerLog($errorMessage);
            }
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        }
    }

    public static function sendForgotPasswordEmail($email, $password)
    {
        try {
            $to_email = $email;
            $subject = 'New password!!!';
            $message = "New password: " . $password;
            $message .= "\n";
            $headers = 'From: noreply@camagru.com';
            $success = mail($to_email, $subject, $message, $headers);
            if (!$success) {
                $errorMessage = error_get_last()['message'];
                mailerLog($errorMessage);
            }
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        }
    }


    public static function generateRandomPassword($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function updateUserPassword($password, $userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE user SET password = :password WHERE id = :userId;';

            $pas = hash('whirlpool', $password);
            $result = $db->prepare($sql);
            $result->bindParam(':password', $pas, PDO::PARAM_INT);
            $result->bindParam(':userId', $userId, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function updateUserName($name, $userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE user SET name = :name WHERE id = :userId;';

            $result = $db->prepare($sql);
            $result->bindParam(':name', $name, PDO::PARAM_INT);
            $result->bindParam(':userId', $userId, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function updateNotification($flag, $userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE user SET notification = :notification WHERE id = :userId;';

            $result = $db->prepare($sql);
            $result->bindParam(':notification', $flag, PDO::PARAM_INT);
            $result->bindParam(':userId', $userId, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function updateUserEmail($email, $userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE user SET email = :email WHERE id = :userId;';

            $result = $db->prepare($sql);
            $result->bindParam(':email', $email, PDO::PARAM_INT);
            $result->bindParam(':userId', $userId, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function checkUserDataByToken($email, $password, $token)
    {
        try {
            $db = Db::getConnection();
            $sql = "SELECT u.id FROM user  as u JOIN access_token as a on u.id = a.user_id WHERE u.email= :email AND u.password = :password AND a.token = :token;";

            $pas = hash('whirlpool', $password);
            $result = $db->prepare($sql);
            $result->bindParam(':email', $email, PDO::PARAM_INT);
            $result->bindParam(':password', $pas, PDO::PARAM_INT);
            $result->bindParam(':token', $token, PDO::PARAM_INT);
            $result->execute();
            $user = $result->fetch();
            return $user ? $user['id'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function updateStatusUser($userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE user SET status = 1 WHERE id = :id;';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $userId, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function updateStatusToken($token)
    {
        try {
            $db = Db::getConnection();
            $sql = 'UPDATE access_token SET status = 1 WHERE token = :token;';

            $result = $db->prepare($sql);
            $result->bindParam(':token', $token, PDO::PARAM_INT);
            $result = $result->execute();
            return $result;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function checkToken($token)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM access_token WHERE token = :token';

            $result = $db->prepare($sql);
            $result->bindParam(':token', $token);
            $result->execute();
            $token = $result->fetch();
            return $token ? $token['status'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getUserIdByEmail($email)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE email = :email';

            $result = $db->prepare($sql);
            $result->bindParam(':email', $email);
            $result->execute();
            $user = $result->fetch();
            return $user ? $user['id'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getUserEmailById($userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $userId);
            $result->execute();
            $user = $result->fetch();
            return $user ? $user['email'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function getUserById($userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $userId);
            $result->execute();
            $user = $result->fetch();
            return $user ? $user : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

    public static function checkNotification($userId)
    {
        try {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM user WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $userId);
            $result->execute();
            $notification = $result->fetch();
            return $notification ? $notification['notification'] : false;
        } catch (\Exception $e) {
            reportLog($e->getMessage());
        } catch (PDOException $db) {
            reportLog($db->getMessage());
        }
    }

}
