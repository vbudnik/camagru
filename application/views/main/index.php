<h3>Log in</h3>
<div class="error" id="error-message">
    <ul id="error-message-list" style="background-color: transparent;"></ul>
</div>

<div class="login-form">
    <form action="javascript:void(0);" method="post">
        <label for="email">E-mail</label>
        <input type="email" name="email" id="email" placeholder="E-mail" value=""/>
        <label for="password">Password</label>
        <input type="password" name="password" id="password" placeholder="Password" value=""/>
        <br>
        <center>
            <input type="submit" name="submit" id="submit" value="Login"/>
        </center>
    </form>
</div>

<div class="login-link">
    <div class="registerbtn">
        <a href="<?php echo BASE_URL ?>account/register">
            <button type="submit" class="signupbtn">Register</button>
        </a>
    </div>
    <div class="signin">
        <a href="<?php echo BASE_URL ?>account/forgot"><p>Forget password?</p></a>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("click", function() {
        var error_list = document.getElementById('error-message-list');
        if (error_list.innerText.length !== 0) {
            error_list.innerText = '';
        }
    });
    document.getElementById("submit").addEventListener("click", function(event) {
        event.preventDefault();
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        if (email === "" || password === "") {
            alert("Not insert user data!!!");
            return false;
        }
        var data = "email="+email+"&password="+password;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    location.reload();
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    var i = 0;
                    while (i < response.length) {
                        error_list.innerHTML += "<li id='error-msg'>" + response[i] +"</li><br>";
                        i++;
                    }
                }
            }
        };
    });
</script>