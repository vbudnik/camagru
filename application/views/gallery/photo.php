
<h3>Photo gallery</h3>
<hr>
<div class="content">
    <div class="back-btm">
    <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
    <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
        <?php else: ?>
        <a href="/gallery">
            <?php endif; ?>
            <button type="submit" class="signupbtn">&#10094; Back</button>
        </a>
    </div>

    <div class="image">
        <img class="gallery-image" src='<?php echo $vars['img']; ?>' width="500" height="500">
    </div>
    <style>
        .button-like {
            border: 2px solid #8a8a8a;
            background-color: transparent;
            text-decoration: none;
            padding: 7px;
            position: relative;
            vertical-align: middle;
            text-align: center;
            display: inline-block;
            border-radius: 3rem;
            color: #8a8a8a;
            transition: all ease 0.4s;
        }

        .button-like span {
            margin-left: 0.5rem;
        }

        .button-like span {
            transition: all ease 0.4s;
        }

        .button-like:focus {
            background-color: transparent;
        }

        .button-like:focus span {
            color: #8a8a8a;
        }

        .button-like:hover {
            border-color: #cc4b37;
            background-color: transparent;
        }

        .button-like:hover span {
            color: #cc4b37;
        }

        .liked {
            background-color: #cc4b37;
            border-color: #cc4b37;
        }

        .liked span {
            color: #fefefe;
        }

        .liked:focus {
            background-color: #cc4b37;
        }

        .liked:focus span {
            color: #fefefe;
        }

        .liked:hover {
            background-color: #cc4b37;
            border-color: #cc4b37;
        }

        .liked:hover span {
            color: #fefefe;
        }

    </style>

    <div class="likes-form">
        <div class="like"><p><strong>Likes: <span id="like"><?php echo $vars['like'] ?></span></strong></p></div>
        <button class="button button-like <?php if ($vars['if_user_like'] == 1):?>liked<?php endif; ?>" id="like-button">
            <span>Like</span>
        </button>
    </div>

    <div class="comment" id="comment">
        <?php $encodeData = json_decode($vars['data']); ?>
        <?php foreach ($encodeData as $key => $val): ?>
            <p class="author">Author :
                <?php if (strlen($val->name_sender)): ?>
                    <?php echo $val->name_sender ?>
                <?php else: ?>
                    <?php echo "DELETE" ?>
                <?php endif; ?></p>
            <p class="comment">Comment : <?php echo $val->comment_text ?></p>
        <?php endforeach; ?>
    </div>
    <div class="message" id="message">
        <ul id="message-list" style="background-color: transparent;"></ul>
    </div>

    <div class="comment-form">
        <form action="javascript:void(0);" method="post">
            <label for="subject">Add Comment</label>
            <textarea id="subject" name="subject" placeholder="Write something.."></textarea>
            <input type="submit" name="submit" value="Add Comment" id="submit"/>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener("click", function() {
        var error_list = document.getElementById('message-list');
        if (error_list.getElementsByTagName('li').length > 1) {
            error_list.innerText = '';
        }
        if (error_list.innerText.length !== 0) {
            setTimeout(function () {
                error_list.innerText = '';
            }, 5000);
        }
    });

    document.getElementById("like-button").addEventListener("click", function(event) {
        event.preventDefault();
        var liked = 0;
        var b = document.getElementById('like-button');
        if (b.classList.contains('liked')) {
            b.classList.remove('liked');
        } else {
            b.classList.add('liked');
            liked = 1;
        }

        var data = "likes=" + liked + "&url=" + document.URL;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>like", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length !== 0) {
                    var val = document.getElementById('like').innerText;
                    console.log(val);
                    if (val === '0') {
                        document.getElementById('like').innerHTML = 1;
                    } else {
                        document.getElementById('like').innerHTML = parseInt(val) + 1;
                    }
                } else {
                    var val = document.getElementById('like').innerText;
                    if (val !== '0') {
                        document.getElementById('like').innerHTML = parseInt(val) - 1;
                    }
                }
            }
        };
    });

    document.getElementById("submit").addEventListener("click", function(event) {
        event.preventDefault();
        var error_list = document.getElementById('message-list');

        var comment = document.getElementById("subject").value;
        var errors = false;

        if ((comment.trim()).length === 0) {
            error_list.innerHTML += "<li id='error-msg'>Comment can't be empty!!!</li><br>";
            errors = true;
        }

        if (errors) {
            document.getElementById("submit").disabled = true;
            setTimeout(() => {
                document.getElementById("submit").disabled = false;
            }, 5000);
            return false;
        }

        var data = "subject=" + comment+"&url="+document.URL;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>addComment", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length !== 0) {
                    var response = JSON.parse(xhr.responseText);
                    var error_list = document.getElementById('message-list');
                    var commentDiv = document.getElementById('comment');
                    commentDiv.innerHTML +=  '<p class="author">Author : ' + response.author +'</p>';
                    commentDiv.innerHTML +=  '<p class="comment">Comment : ' + response.comment +'</p>';
                    error_list.innerHTML += "<li id='error-msg'><strong>Your comment will be added!</strong></li><br>";
                    document.getElementById("subject").value = "";
                } else {
                    var error_list = document.getElementById('message-list');
                    error_list.innerHTML += "<li id='error-msg'><strong>Problem</strong></li><br>";
                }
            }
        };
    });
</script>
