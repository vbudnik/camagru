<h3>Change account data!</h3>
<hr>
<div class="error" id="error-message">
    <ul id="error-message-list" style="background-color: transparent;"></ul>
</div>
<hr>
<div class="change-form">
    <form action="javascript:void(0);" method="post">
        <div class="change">
            <label for="name">Name</label>
            <input type="text" placeholder="Enter Name" name="name" id="name" value="<?php echo $vars['name'] ?>">
            <button type="button" id="change-name" name="button">Change Name</button>
        </div>
        <div class="change">
            <label for="email">Email</label>
            <input type="text" placeholder="Enter Email" name="email" id="email" value="<?php echo $vars['email'] ?>">
            <button type="button" id="change-email" name="button">Change Email</button>
        </div>
        <div class="change">
            <label for="name">Notification</label>
            <input type="checkbox" placeholder="Send Notification" name="name" id="notification" <?php if ($vars['notification'] == 1): ?>checked <?php endif; ?>>
            <button type="button" id="change-notification" name="button">Save Notification</button>
        </div>
        <div class="change">
            <label for="psw">Password</label>
            <input type="password" placeholder="Enter Password" name="psw" id="psw" autocomplete="off">
            <button type="button" id="change-password" name="button">Change Password</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    document.addEventListener("click", function() {
        var error_list = document.getElementById('error-message-list');
        if (error_list.innerText.length !== 0) {
            setTimeout(() => {
                error_list.innerText = '';
            }, 5000);
        }
    });

    document.getElementById("change-notification").addEventListener("click", function(event) {
        event.preventDefault();
        var is_checked = document.getElementById('notification').checked;
        var check = 0;
        console.log(is_checked);
        if (is_checked === true) {
            check = 1;
        }

        var data = "checked=" + check;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>account/settingajax", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    var error_list = document.getElementById('error-message-list');
                    error_list.innerHTML += "<li id='error-msg'><strong>Your subscription will be changed!</strong></li><br>";
                    document.getElementById("change-notification").disabled = true;
                    setTimeout(() => {
                        document.getElementById("change-notification").disabled = false;
                    }, 3000);
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });

    document.getElementById("change-name").addEventListener("click", function(event) {
        event.preventDefault();
        var error_list = document.getElementById('error-message-list');
        var name = document.getElementById("name").value;
        var errors = false;

        if ((name.trim()).length === 0) {
            error_list.innerHTML += "<li id='error-msg'>Name can't be empty!!!</li><br>";
            errors = true;
        }

        if (errors) {
            document.getElementById("change-name").disabled = true;
            setTimeout(() => {
                document.getElementById("change-name").disabled = false;
            }, 3000);
            return false;
        }

        var data = "name="+name;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>account/settingajax", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    var error_list = document.getElementById('error-message-list');
                    error_list.innerHTML += "<li id='error-msg'><strong>Your name will be changed!</strong></li><br>";
                    document.getElementById("name").value = '';
                    document.getElementById("change-name").disabled = true;
                    setTimeout(() => {
                        document.getElementById("change-name").disabled = false;
                    }, 3000);
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });

    document.getElementById("change-email").addEventListener("click", function(event) {
        event.preventDefault();
        var error_list = document.getElementById('error-message-list');
        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var email = document.getElementById("email").value;
        var errors = false;

        if ((email.trim()).length === 0 || !testEmail.test(email)) {
            error_list.innerHTML += "<li id='error-msg'>Please check your email!!!</li><br>";
            errors = true;
        }

        if (errors) {
            document.getElementById("change-email").disabled = true;
            setTimeout(() => {
                document.getElementById("change-email").disabled = false;
            }, 3000);
            return false;
        }

        var data = "email="+email;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>account/settingajax", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    var error_list = document.getElementById('error-message-list');
                    error_list.innerHTML += "<li id='error-msg'><strong>Your e-mail will be changed!</strong></li><br>";
                    document.getElementById("email").value = '';
                    document.getElementById("change-email").disabled = true;
                    setTimeout(() => {
                        document.getElementById("change-email").disabled = false;
                    }, 3000);
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });

    document.getElementById("change-password").addEventListener("click", function(event) {
        event.preventDefault();
        var error_list = document.getElementById('error-message-list');
        var password = document.getElementById("psw").value;
        var errors = false;

        if ((password.trim()).length === 0 || ((password.trim()).length <= 4)) {
            error_list.innerHTML += "<li id='error-msg'>Please check your password!!!</li><br>";
            errors = true;
        }

        if (errors) {
            document.getElementById("change-password").disabled = true;
            setTimeout(() => {
                document.getElementById("change-password").disabled = false;
            }, 3000);
            return false;
        }

        var data = "password=" + password;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', "<?php echo BASE_URL ?>account/settingajax", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                if (xhr.response.length === 0) {
                    var error_list = document.getElementById('error-message-list');
                    error_list.innerHTML += "<li id='error-msg'><strong>Your password will be changed!</strong></li><br>";
                    document.getElementById("psw").value = '';
                    document.getElementById("change-password").disabled = true;
                    setTimeout(() => {
                        document.getElementById("change-password").disabled = false;
                    }, 3000);
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });
</script>