<h3>Register</h3>
<p>Please fill in this form to create an account.</p>
<hr>

<div class="error" id="error-message">
    <ul id="error-message-list" style="background-color: transparent;"></ul>
</div>
<hr>
<div class="login-form">
    <form action="javascript:void(0);" method="post">
        <label for="name">Name</label>
        <input type="text" placeholder="Enter Name" name="name" id="name">
        <label for="email">Email</label>
        <input type="text" placeholder="Enter Email" name="email" id="email">
        <label for="psw">Password</label>
        <input type="password" placeholder="Enter Password" name="psw" id="psw">
        <label for="pswrepeat">Repeat Password</label>
        <input type="password" placeholder="Repeat Password" name="pswrepeat" id="pswrepeat">
        <center>
            <input type="submit" name="submit" value="Register" id="submit"/>
        </center>
    </form>
</div>


<div class="signin">
	<p>Already have an account? <a href="/">Sign in</a></p>
</div>

<script type="text/javascript">
    document.addEventListener("click", function() {
        var error_list = document.getElementById('error-message-list');
        if (error_list.innerText.length !== 0) {
            setTimeout(() => {
            error_list.innerText = '';
            }, 5000);
        }
    });

    document.getElementById("submit").addEventListener("click", function(event) {
        event.preventDefault();
        var error_list = document.getElementById('error-message-list');
        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var psw = document.getElementById("psw").value;
        var pswrepeat = document.getElementById("pswrepeat").value;
        var errors = false;

        if ((name.trim()).length === 0) {
            error_list.innerHTML += "<li id='error-msg'>Name can't be empty!!!</li><br>";
            errors = true;
        }
        if ((email.trim()).length === 0 || !testEmail.test(email)) {
            error_list.innerHTML += "<li id='error-msg'>Please check your email!!!</li><br>";
            errors = true;
        }

        if ((psw.trim()).length === 0 || (pswrepeat.trim()).length === 0
            || (pswrepeat.trim()).length !== (psw.trim()).length) {
            error_list.innerHTML += "<li id='error-msg'>Please check your password!!!</li><br>";
            errors = true;
        }

         if (errors) {
             document.getElementById("submit").disabled = true;
             setTimeout(() => {
                 document.getElementById("submit").disabled = false;
             }, 5000);
             return false;
         }

         var data = "email="+email+"&name="+name+"&psw="+psw+"&pswrepeat="+pswrepeat;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', "/account/register", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
        xhr.onload = function() {
            if (xhr.status != 200) {
                alert("Problem with server! Contact with developer!!!")
            } else {
                console.log(xhr.response.length);
                console.log(xhr.responseText);
                if (xhr.response.length === 0) {
                    var error_list = document.getElementById('error-message-list');
                    error_list.innerHTML += "<li id='error-msg'><strong>Please check your email, for confirm registration!!!</strong></li><br>";
                    setTimeout(() => {
                        window.location.href = "<?php echo BASE_URL ?>";
                    }, 5000);
                } else {
                    var error_list = document.getElementById('error-message-list');
                    var response = JSON.parse(xhr.responseText);
                    error_list.innerHTML += "<li id='error-msg'>" + response +"</li><br>";
                }
            }
        };
    });
</script>