<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>/public/styles/style.css">
    <title>Page Not found</title>
</head>
<body>
<div class="container">
    <div class="topnav">
        <a class="active" href="<?php echo BASE_URL ?>">Camagru</a>
        <a href="<?php echo BASE_URL ?>">Home</a>
        <a href="<?php echo BASE_URL ?>">Contact</a>
        <a href="<?php echo BASE_URL ?>">About</a>
    </div>
    <div class="error">
        <img src="<?php echo BASE_URL ?>/public/img/404_error-illo.png"/>
        <h1>Error 403</h1>
        <p>We can't find the page you're looking for.</p>
    </div>
</div>
<div class="footer">
    <hr>
    <p>&copy;2018 vbudnik<p>
</div>
</body>
</html>
