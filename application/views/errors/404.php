<div class="error">
    <img src="<?php echo BASE_URL ?>/public/img/errors_msg_img/404_error-illo.png"/>
    <h1>Error (404)</h1>
    <p>We can't find the page you're looking for.</p>
</div>
